In this project, I tried to implement an onion architecture using several patterns, which in the future should provide good maintenance.

### Required features

- Display a list of recipes (date created, description)
- Add a new recipe
- Add a new recipe based on another one - culinary “fork” of a recipe (e.g. Fried Chicken -> Fried Chicken with Mayo -> Fried Chicken with Mayo and Mustard). Child recipes do not inherit anything from a parent recipe - they are just shown as children of a parent one. User can add child recipes to recipes on any depth
- Modify existing recipe (on any depth)
- View all previous recipe versions (on any depth)
- Recipes should be sorted alphabetically (on any depth)

On the backend was implemented all cases except versioning (without unit tests).

On the frontend there only a home component with a list of the root element (**Cookbook**) and first-level depth recipes.
