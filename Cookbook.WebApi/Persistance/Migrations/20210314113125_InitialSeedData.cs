﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Persistence.Migrations
{
    public partial class InitialSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("Recipes",
                new[] { "Id", "Title", "Description", "CreationDate", "HierarchyId" },
                new object[,]
                {
                    { Guid.NewGuid(), "Cookbook", null, DateTime.UtcNow.Date, HierarchyId.GetRoot() }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var tableToTruncate = "Recipes";

            migrationBuilder.Sql($"TRUNCATE TABLE {tableToTruncate};");
        }
    }
}
