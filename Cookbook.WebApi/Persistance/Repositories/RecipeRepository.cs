﻿using Domain.Entities;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class RecipeRepository : IRecipeRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public RecipeRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Add(Recipe recipe)
        {
            await _dbContext.Recipes.AddAsync(recipe);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Recipe recipe)
        {
            _dbContext.Recipes.Remove(recipe);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Recipe> GetRecipeById(Guid id) => 
            await _dbContext.Recipes.FirstOrDefaultAsync(r => r.Id == id);

        public async Task<IEnumerable<Recipe>> GetAllRecipes() => 
            await _dbContext.Recipes
                .AsNoTracking()
                .ToArrayAsync();

        public async Task Update(Recipe recipe)
        {
            _dbContext.Update(recipe);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Recipe>> GetRecipes(Expression<Func<Recipe, bool>> expression) => 
            await _dbContext.Recipes
                .AsNoTracking()
                .Where(expression)
                .OrderBy(r => r.Title)
                .ToArrayAsync();
    }
}
