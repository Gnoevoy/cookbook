﻿using Application.Features.RecipeFeatures.Commands;
using Application.Features.RecipeFeatures.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class RecipeController : BaseApiController
    {
        /// <summary>
        /// Creates a new recipe.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateRecipeCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Gets root and frist level recipes.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetRootAndFirstLevelRecipes()
        {
            return Ok(await Mediator.Send(new GetRootAndFirstLevelRecipesQuery()));
        }

        /// <summary>
        /// Gets child recipes based on parent Id.
        /// </summary>
        /// /// <param name="parentId"></param>
        /// <returns></returns>
        [HttpGet("child-recipes/{parentId}")]
        public async Task<IActionResult> GetRecipesByParentIdQuery(Guid parentId)
        {
            return Ok(await Mediator.Send(new GetRecipesByParentIdQuery { ParentId = parentId }));
        }

        /// <summary>
        /// Gets recipe by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            return Ok(await Mediator.Send(new GetRecipeByIdQuery { Id = id }));
        }

        /// <summary>
        /// Deletes recipe based on Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await Mediator.Send(new DeleteRecipeByIdCommand { Id = id }));
        }

        /// <summary>
        /// Updates the recipe based on Id.   
        /// </summary>
        /// <param name="id"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("[action]")]
        public async Task<IActionResult> Update(Guid id, UpdateRecipeCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }
    }
}
