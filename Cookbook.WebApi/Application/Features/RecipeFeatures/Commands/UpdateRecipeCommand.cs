﻿using Domain.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.RecipeFeatures.Commands
{
    public class UpdateRecipeCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }

        public class UpdateRecipeCommandHandler : IRequestHandler<UpdateRecipeCommand, Guid>
        {
            private readonly IRecipeRepository _recipeRepository;

            public UpdateRecipeCommandHandler(IRecipeRepository recipeRepository)
            {
                _recipeRepository = recipeRepository;
            }

            public async Task<Guid> Handle(UpdateRecipeCommand command, CancellationToken cancellationToken)
            {
                var recipe = await _recipeRepository.GetRecipeById(command.Id);

                if (recipe == null)
                {
                    return default;
                }
                else
                {
                    recipe.Title = command.Title;
                    recipe.Description = command.Description;
                    recipe.CreationDate = command.CreationDate;

                    await _recipeRepository.Update(recipe);
                    return command.Id;
                }
            }
        }
    }
}
