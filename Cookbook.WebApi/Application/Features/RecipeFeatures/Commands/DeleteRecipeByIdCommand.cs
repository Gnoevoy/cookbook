﻿using Domain.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.RecipeFeatures.Commands
{
    public class DeleteRecipeByIdCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }

        public class DeleteRecipeByIdCommandHandler : IRequestHandler<DeleteRecipeByIdCommand, Guid>
        {
            private readonly IRecipeRepository _recipeRepository;

            public DeleteRecipeByIdCommandHandler(IRecipeRepository recipeRepository)
            {
                _recipeRepository = recipeRepository;
            }

            public async Task<Guid> Handle(DeleteRecipeByIdCommand command, CancellationToken cancellationToken)
            {
                var recipe = await _recipeRepository.GetRecipeById(command.Id);

                if (recipe == null)
                {
                    return default;
                }
                else
                {
                    await _recipeRepository.Delete(recipe);
                    return command.Id;
                }
            }
        }
    }
}
