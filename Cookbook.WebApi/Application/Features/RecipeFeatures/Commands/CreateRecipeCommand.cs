﻿using Domain.Entities;
using Domain.Interfaces;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.RecipeFeatures.Commands
{
    public class CreateRecipeCommand : IRequest<Guid>
    {
        public Guid ParentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public class CreateRecipeCommandHandler : IRequestHandler<CreateRecipeCommand, Guid>
        {
            private readonly IRecipeRepository _recipeRepository;

            public CreateRecipeCommandHandler(IRecipeRepository recipeRepository)
            {
                _recipeRepository = recipeRepository;
            }

            public async Task<Guid> Handle(CreateRecipeCommand command, CancellationToken cancellationToken)
            {
                var parentRecipe = await _recipeRepository.GetRecipeById(command.ParentId);
                var maxHid = (await _recipeRepository
                    .GetRecipes(r => r.HierarchyId.GetAncestor(1) == parentRecipe.HierarchyId))
                    .Max(r => r.HierarchyId);

                var recipe = new Recipe
                {
                    Title = command.Title,
                    Description = command.Description,
                    CreationDate = DateTime.Now.Date,
                    HierarchyId = parentRecipe.HierarchyId.GetDescendant(maxHid, null)
                };

                await _recipeRepository.Add(recipe);

                return recipe.Id;
            }
        }
    }
}
