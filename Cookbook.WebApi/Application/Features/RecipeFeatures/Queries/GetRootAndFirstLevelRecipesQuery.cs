﻿using Application.ViewModels.RecipeViewModels;
using Domain.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.RecipeFeatures.Queries
{
    public class GetRootAndFirstLevelRecipesQuery : IRequest<IEnumerable<BaseRecipeViewModel>>
    {
        public class GetRootAndFirstLevelRecipiesQueryHandler : IRequestHandler<GetRootAndFirstLevelRecipesQuery, IEnumerable<BaseRecipeViewModel>>
        {
            private readonly IRecipeRepository _recipeRepository;

            public GetRootAndFirstLevelRecipiesQueryHandler(IRecipeRepository recipeRepository)
            {
                _recipeRepository = recipeRepository;
            }

            public async Task<IEnumerable<BaseRecipeViewModel>> Handle(GetRootAndFirstLevelRecipesQuery query, CancellationToken cancellationToken) =>
                await GetMappedRecipes();

            private async Task<IEnumerable<BaseRecipeViewModel>> GetMappedRecipes()
            {
                var resultRecipes = new List<BaseRecipeViewModel>();
                var domainRecipes = await _recipeRepository.GetRecipes(r =>
                                    r.HierarchyId == HierarchyId.GetRoot() ||
                                    r.HierarchyId.GetAncestor(1) == HierarchyId.GetRoot());

                foreach (var domainRecipe in domainRecipes)
                {
                    var recipe = new BaseRecipeViewModel
                    {
                        Id = domainRecipe.Id,
                        Title = domainRecipe.Title,
                        HierarchyId = domainRecipe.HierarchyId.ToString()
                    };

                    resultRecipes.Add(recipe);
                }

                return resultRecipes;
            }
        }
    }
}
