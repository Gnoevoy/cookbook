﻿using Application.ViewModels.RecipeViewModels;
using Domain.Entities;
using Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.RecipeFeatures.Queries
{
    public class GetRecipesByParentIdQuery : IRequest<IEnumerable<BaseRecipeViewModel>>
    {
        public Guid ParentId { get; set; }

        public class GetRecipesByParentIdQueryHandler : IRequestHandler<GetRecipesByParentIdQuery, IEnumerable<BaseRecipeViewModel>>
        {
            private readonly IRecipeRepository _recipeRepository;

            public GetRecipesByParentIdQueryHandler(IRecipeRepository recipeRepository)
            {
                _recipeRepository = recipeRepository;
            }

            public async Task<IEnumerable<BaseRecipeViewModel>> Handle(GetRecipesByParentIdQuery query, CancellationToken cancellationToken)
            {
                var parentRecipe = await _recipeRepository.GetRecipeById(query.ParentId);

                return await GetMappedRecipes(parentRecipe);
            }

            private async Task<IEnumerable<BaseRecipeViewModel>> GetMappedRecipes(Recipe parentRecipe)
            {
                var resultRecipes = new List<BaseRecipeViewModel>();
                var domainRecipes = await _recipeRepository
                    .GetRecipes(r => r.HierarchyId.GetAncestor(1) == parentRecipe.HierarchyId);

                foreach (var domainRecipe in domainRecipes)
                {
                    var recipe = new BaseRecipeViewModel
                    {
                        Id = domainRecipe.Id,
                        Title = domainRecipe.Title,
                        HierarchyId = domainRecipe.HierarchyId.ToString()
                    };

                    resultRecipes.Add(recipe);
                }

                return resultRecipes;
            }
        }
    }
}
