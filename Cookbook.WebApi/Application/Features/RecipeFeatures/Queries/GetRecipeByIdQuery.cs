﻿using Application.ViewModels.RecipeViewModels;
using Domain.Entities;
using Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.RecipeFeatures.Queries
{
    public class GetRecipeByIdQuery : IRequest<RecipeDetailsVeiwModel>
    {
        public Guid Id { get; set; }

        public class GetProductByIdQueryHandler : IRequestHandler<GetRecipeByIdQuery, RecipeDetailsVeiwModel>
        {
            private readonly IRecipeRepository _recipeRepository;

            public GetProductByIdQueryHandler(IRecipeRepository recipeRepository)
            {
                _recipeRepository = recipeRepository;
            }

            public async Task<RecipeDetailsVeiwModel> Handle(GetRecipeByIdQuery query, CancellationToken cancellationToken)
            {
                var domainRecipe = await _recipeRepository.GetRecipeById(query.Id);

                return new RecipeDetailsVeiwModel
                {
                    Id = domainRecipe.Id,
                    Title = domainRecipe.Title,
                    HierarchyId = domainRecipe.HierarchyId.ToString(),
                    Description = domainRecipe.Description
                };
            }
        }
    }
}
