﻿using System;

namespace Application.ViewModels.RecipeViewModels
{
    public class BaseRecipeViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string HierarchyId { get; set; }
    }
}
