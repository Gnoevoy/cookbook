﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IRecipeRepository
    {
        Task<IEnumerable<Recipe>> GetAllRecipes();
        Task<IEnumerable<Recipe>> GetRecipes(Expression<Func<Recipe, bool>> expression);
        Task<Recipe> GetRecipeById(Guid id);
        Task Add(Recipe recipe);
        Task Update(Recipe recipe);
        Task Delete(Recipe recipe);
    }
}
