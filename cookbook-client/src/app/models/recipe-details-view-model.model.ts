import { BaseRecipeViewModel } from './base-recipe-view-model.model';

export class RecipeDetailsViewModel extends BaseRecipeViewModel {
  description = '';
  parentId = '';
}
