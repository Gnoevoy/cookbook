import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseRecipeViewModel } from '../models/base-recipe-view-model.model';
import { RecipeService } from '../services/recipe.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  recipes: Observable<BaseRecipeViewModel[]>;
  createMode = false;

  constructor(public recipeService: RecipeService) {
    this.recipes = new Observable();
  }

  ngOnInit(): void {
    this.loadRecipes();
  }

  loadRecipes(): void {
    this.recipes = this.recipeService.getRootAndFirstLevelRecipes();
  }
}
