import { RecipeDetailsViewModel } from './../models/recipe-details-view-model.model';
import { Injectable } from '@angular/core';
import { environment as config } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { BaseRecipeViewModel } from '../models/base-recipe-view-model.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  private RECIPES_RESOURCE = config.baseUrl + 'recipe';

  constructor(private http: HttpClient) { }

  getRootAndFirstLevelRecipes(): Observable<BaseRecipeViewModel[]> {
    return this.http.get<BaseRecipeViewModel[]>(this.RECIPES_RESOURCE);
  }
}
