import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NavbarComponent } from './navbar/navbar.component';



@NgModule({
  declarations: [ToolbarComponent, NavbarComponent],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
